package com.example.amirreza.hellowebbroswer;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import static java.lang.Thread.sleep;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {

                    //redirect to getPermission
                    Intent intent = new Intent(SplashActivity.this, GetPermissionActivity.class);
                    startActivity(intent);
                } else {

                    //redirect to Browser Activity
                    Intent intent = new Intent( SplashActivity.this, BrowserActivity.class);
                    startActivity(intent);
                }

            }
        }, 3000);

    }
}
