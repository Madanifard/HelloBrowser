package com.example.amirreza.hellowebbroswer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BrowserActivity extends AppCompatActivity {

    EditText searchTxt;
    Button searchBtn;
    WebView browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        bind();

        browser.getSettings().setJavaScriptEnabled(true);
        browser.setWebViewClient(new WebViewClient());
        browser.loadUrl("https://www.google.com");
    }

    /**
     * for binding widget
     */
    public void bind() {

        searchTxt = (EditText) findViewById(R.id.search_txt);

        searchBtn = (Button) findViewById(R.id.search_btn);

        browser = (WebView) findViewById(R.id.browser);

    }


    /**
     * check if not have http:// or https:// redirect to google search
     * @param data
     * @return
     */
    public String checkEnter(String data) {

        String url = "https://www.google.com";

        if(!data.equals("")) {

            if(data.contains("http://") || data.contains("https://")) {
                url = data;
            } else {
                url = "https://www.google.com/search?q=" + data;
            }

        }

        return url;
    }

    /**
     * Event click for search
     * @param view
     */
    public void onClickSearch(View view) {

        String url = checkEnter(searchTxt.getText().toString());
        
        browser.getSettings().setJavaScriptEnabled(true);
        browser.setWebViewClient(new WebViewClient());
        browser.loadUrl(url);
    }

}
