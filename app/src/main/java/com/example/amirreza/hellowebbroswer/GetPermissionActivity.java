package com.example.amirreza.hellowebbroswer;

import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GetPermissionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_permission);
    }

    /**
     * if click notOk terminate
     * @param view
     */
    public void onClickNotOk(View view) {

        finish();
    }

    /**
     * get permission
     * @param view
     */
    public void onClickOk(View view) {

        ActivityCompat.requestPermissions(GetPermissionActivity.this,
                new String[]{Manifest.permission.INTERNET},
                1500);

        ActivityCompat.requestPermissions(GetPermissionActivity.this,
                new String[]{Manifest.permission.ACCESS_WIFI_STATE},
                1600);

        ActivityCompat.requestPermissions(GetPermissionActivity.this,
                new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                1700);
    }

}
